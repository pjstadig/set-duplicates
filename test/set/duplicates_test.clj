(ns set.duplicates-test
  (:require
   [clojure.test :refer [deftest is]]
   [set.duplicates :refer [-main]]))

(deftest t-main
  (let [ex             (try (-main) (catch Exception e e))
        relevant-trace (filter (comp #{"duplicates.clj"}
                                 (memfn getFileName))
                         (.getStackTrace ex))]
    (is (= 10 (-> relevant-trace first .getLineNumber)))))
