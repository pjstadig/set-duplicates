This is a repro case for an exception thrown that points to the wrong line.

`set.duplicates/-main` will throw an exception because it contains a set literal
that will evaluate with duplicates. Clojure’s compiler does not emit line
numbers for set literals, so the exception’s stacktrace points to the code 4
lines before the line that actually caused the exception.

In this particular example, there are four lines of comments between the
offending set literal and the last line to emit a line number. This can be
confusing even for experienced Clojure developers, but can be particularly
confusing for beginners.

The `~/bin/test` script will run the tests to illustrate the mismatch between
expectations and what actually happens.
