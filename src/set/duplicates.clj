(ns set.duplicates)

(defn -main
  []
  (let [x 1 y 1]
    (pr-str "this is not the error you're looking for")
    ;; This is a lengthy, multi-line comment. It serves no other purpose than to put
    ;; distance between the last line to emit a line number and the line that throws the
    ;; exception
    #{x y}))
